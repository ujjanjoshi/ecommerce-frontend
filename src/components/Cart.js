import axios from "axios";
import { useEffect, useState } from "react";

function Cart(){
    const ids=JSON.parse(localStorage.getItem('dataKey')) ;
    let [quantity,setQuantity]=useState([]);
    const[product,setProduct]=useState([]);
    const productdetail =  async () => {
        axios.post(`${process.env.REACT_APP_API_URL}cartproducttemp`,ids).then((response)=>{
            setProduct(response.data);
            setQuantity(response.data?.map((item)=> {
                return {id: item.id, count: 1}
            }))
        }); 
    };
    const increasequantity=(id)=>{
        setQuantity(quantity.map(item => {
            if(item.id === id) {
                return {...item, count: item.count+1}
            }
            return item
        }))
    }
    const decreasequantity=(id)=>{
        setQuantity(quantity.map(item => {
            if(item.id === id) {
                if(item.count >0){
                    return {...item, count: item.count-1}
                }
                else{
                    return {...item, count: 0}
                }
               
            }
            return item
        }))
    }
    useEffect(()=>{
        productdetail();
    },[]);

    const total = () => {
        let total = 0
        product.map(item => {
            total+= item.price * quantity.find(v => v.id === item.id).count
        })
        return total
    } 
    console.log(total());

 return(
    <>
     <div className="card-body" style={{margin:20}}>
            <table id="dtBasicExample" className="table  table-sm" cellspacing="0" width="100%" >
                <thead>
                  <tr>
                    <th className="th-sm">S.No
                    </th>
                    <th className="th-sm">Image
                    </th>
                    <th className="th-sm">Title
                    </th>
                    <th className="th-sm">Quantity
                    </th>
                    <th className="th-sm">Price
                    </th>
                    <th className="th-sm">Total Price
                    </th>
                  </tr>
                </thead>
                <tbody>
                {product?.map((e)=>
                    <tr>
                    <td >{e.id}</td>
                    <td><img src={`${process.env.REACT_APP_API_URL}getimage/${e.image}`} alt="productimg" width="40px"/></td>
                    <td>{e.title}</td>
    
                    <td>
                        <button className="btn btn-secondary" onClick={()=>increasequantity(e.id)} style={{height:30,marginRight:5,padding:5}}>+</button>
                        <input type="text"  style={{width:30,textAlign:"center"}} value={quantity.find(v => v.id === e.id).count}></input>
                        <button className="btn btn-secondary" onClick={()=>decreasequantity(e.id)} style={{height:30,marginLeft:5,padding:5}}>-</button>
                        </td>
                    <td>Rs. {e.price}</td>
                    <td>Rs. { quantity.find(v => v.id === e.id).count*e.price}</td>
                    </tr>
                     )}
                     <tr>
                        <td colSpan={4} ></td>
                        <td  >Grand Total</td>
                        <td>Rs {total()}</td>
                     </tr>
                </tbody>
              </table>
        </div>
    </>
 )   
}
export default Cart;