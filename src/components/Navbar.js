import { DeviceMobile, ShoppingCart } from "phosphor-react";
import { useState } from "react";
import { Link, useLocation } from "react-router-dom";
function Navbar() {
  const location = useLocation();
  const checkLocalStorage = (key) => {
    const value = localStorage.getItem(key);
    if(value !== null){
      return value;
    }else{
      return "Login";
    }
  };
  const hasValue = checkLocalStorage('username');
  const checkLocalStorageCart = (key) => {
    const value = JSON.parse(localStorage.getItem(key));
    if(value !== null){
      return value.length;
    }else{
      return 0;
    }
  };

  let length=checkLocalStorageCart('dataKey');
    return (
      <nav className="navbar navbar-expand-lg " style={{backgroundColor:'white'}} >
      <div className="container-fluid">
        <Link className="navbar-brand" to="/" style={{color:"grey"}}><DeviceMobile size={32} color="grey"/>MobileShop</Link>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link className="nav-link active" to="/" style={{color:"grey"}} >Product</Link>
            </li>
          </ul>
          <Link to="/cart" style={{color:"grey",marginRight:20}}><ShoppingCart size={32} /><span class="badge bg-secondary">{length}</span></Link>
          <Link type="button" to="/login" class="btn btn-primary btn-sm" style={{backgroundColor:'grey',border:'none',marginRight:10 }}>{hasValue}</Link>
        </div>
      </div>
    </nav>
    );
  }
  
  export default Navbar;
  