import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

function Login() {
    localStorage.removeItem('username');
    const [useremail,setUserEmail]=useState();
    const [password,setPassword]=useState();
    const navigate=useNavigate(); 
    const handleSubmit = async (e) => {
        e.preventDefault()
        const data = {
            useremail,
            password
        }
        await axios.post(`${process.env.REACT_APP_API_URL}login`, data).then((response)=>{
            if(response.data.message=="Login Successful "){
              const username=response.data.username;
             localStorage.setItem('username',username);
                navigate("/",{state:username})
            }
        });
    }

    return (
    <section class="vh-100">
    <div class="container py-5 h-100">
      <div class="row d-flex align-items-center justify-content-center h-100">
        <div class="col-md-8 col-lg-7 col-xl-6">
          <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
            class="img-fluid" alt="Phone image"/>
        </div>
        <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
          <form onSubmit={handleSubmit}>
            <div class="form-outline mb-4">
                <label class="form-label" for="form1Example13">Email address</label>
              <input type="email" name= "useremail" value={useremail} onChange={(e)=>setUserEmail(e.target.value)} id="form1Example13" class="form-control form-control-lg" />
              
            </div>
            <div class="form-outline mb-4">
                <label class="form-label" for="form1Example23">Password</label>
              <input type="password"  name= "password" value={password} onChange={(e)=>setPassword(e.target.value)} id="form1Example23" class="form-control form-control-lg" />
              
            </div>
  
            <div class="d-flex justify-content-around align-items-center mb-4">
            </div>
            <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
            <div class="text-center text-lg-start mt-4 pt-2">
                <p class="small fw-bold mt-2 pt-1 mb-0">Don't have an account? <a href="/register"
                    class="link-danger">Register</a></p>
              </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  )
}
export default Login;