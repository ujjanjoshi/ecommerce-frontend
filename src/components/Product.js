import axios from "axios";
import { ShoppingCartSimple } from "phosphor-react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
function Product(){
    const[product,setProduct]=useState();
    const [data, setData] = useState([]);
    const notify = (id) => {
        console.log(JSON.parse(localStorage.getItem("dataKey") || '[]').includes(id));
        if(!JSON.parse(localStorage.getItem("dataKey") || '[]').includes(id)){
            const id_array=JSON.parse(localStorage.getItem("dataKey") || '[]');
            toast("Sucessfully Added To Cart");
            id_array.push(id);
            setData(id_array);
        }else{
            toast("Already Added To Cart");
        } 
    };
    useEffect(() => {
        data.length && localStorage.setItem('dataKey', JSON.stringify(data));
      }, [data]);
    useEffect(()=>{
        axios.get(`${process.env.REACT_APP_API_URL}product`).then((response)=>{
            setProduct(response.data.product);
        })
    },[]);
    return (
        <>
        <div className="row row-cols-2 row-cols-md-4 g-5" style={{margin:20}}>
  
    {product?.map((e)=>{
    return <div className="col" style={{display:"flex",justifyContent:"space-between"}}>
    <div className="card h-100" style={{width:250}}>
      <img src={`${process.env.REACT_APP_API_URL}getimage/${e.image}`} className="card-img-top" alt="..." width={250} />
  <div className="card-body">
    <h5 className="card-title">{e.title}</h5>
    <p className="card-text">Quantity: {e.quantity}</p>
    <p className="card-text" style={{display:"flex", justifyContent:"space-between"}}>Rs. {e.price} <Link type="button" onClick={()=>notify(e.id)} ><ShoppingCartSimple size={25} color="black"/></Link></p>

  </div>
    </div>
    </div>
    })}
  <ToastContainer theme="light"/>
</div>
        </>
    );
}
export default Product;