// import logo from './logo.svg';
import './App.css';
import Cart from './components/Cart';
import Navbar from './components/Navbar';
import Product from './components/Product';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import Login from './components/login';


function App() {
  return (
    <Router>
    <Navbar/>
    <Routes>
      <Route path='/' element={<Product/>} />
      <Route path='/cart' element={<Cart/>} />
      <Route path='/login' element={<Login/>} />
    </Routes>
  </Router>
  );
}

export default App;
